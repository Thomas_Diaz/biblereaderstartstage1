package bibleReader.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;
import bibleReader.model.Verse;


/*
 * Tests for the Verse class.
 * @author ?
 */

public class Stage01StudentVerseTest {
	
	private Verse ruth1_2;
	private Verse gen3_23;
	private Verse rev11_4;
	
	@Before
	public void setUp() throws Exception {
		ruth1_2 = new Verse(new Reference(BookOfBible.Ruth, 1, 2), "ruth1_2.txt");
		gen3_23 = new Verse(new Reference(BookOfBible.Genesis, 3, 23), "gen3_23.txt");
		rev11_4 = new Verse(new Reference(BookOfBible.Revelation, 11,4), "rev11_4.txt");
		
	}

	/*
	 * Anything that should be done at the end of each test.  
	 * Most likely you won't need to do anything here.
	 */
	@After
	public void tearDown() throws Exception {
		// You probably won't need anything here.
	}

	/*
	 * Add as many test methods as you think are necessary. Two suggestions (without implementation) are given below.
	 */
	
	@Test
	public void testEquals() {
		Verse a = ruth1_2;
		Verse b = a;
		assertEquals(ruth1_2.equals(ruth1_2), true);
		assertEquals(a.equals(b), true);
		assertEquals(ruth1_2.equals(gen3_23), false);
		assertEquals(gen3_23.equals(rev11_4), false);
	}
	
	@Test
	public void testHashCode() {
		assertEquals(ruth1_2.hashCode() == ruth1_2.hashCode(), true);
		assertEquals(ruth1_2.hashCode()!= gen3_23.hashCode(), true);
		assertEquals(gen3_23.hashCode() == rev11_4.hashCode(), false);
	}
	
	
}
