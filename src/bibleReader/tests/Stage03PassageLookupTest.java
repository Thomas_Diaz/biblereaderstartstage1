package bibleReader.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Stage03PassageLookupTest {
	private String exo;
	private String exodus;
	private String ex;
	private String neh;
	private String oneKings;
	private String presentations;
	private String badTwoKings;
	private String badPhlm;
	
	public void setUp() throws Exception {
		exo = "exo";
		exodus = "exodus";
		ex = "ex";
		neh = "neh 2:3-7";
		oneKings = "1Kings 7-9";
		presentations = "presentations 3:54";
		badTwoKings = "2ki 17:978";
		badPhlm = "Phlm 456";
	}

	@Test
	void testisValid() {
		assertTrue(isValid(exo));
		assertTrue(isValid(exodus));
		assertTrue(isValid(ex));
		assertTrue(isValid(neh));
		assertTrue(isValid(oneKings));
		assertFalse(isValid(presentations));
		assertFalse(isValid(badTwoKings));
		assertFalse(isValid(badPhlm));
	}

}
