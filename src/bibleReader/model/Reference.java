package bibleReader.model;

/**
 * A simple class that stores the book, chapter number, and verse number.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Thomas Diaz
 */
public class Reference implements Comparable<Reference> {
	BookOfBible b;
	int c;
	int v;

	public Reference(BookOfBible book, int chapter, int verse) {
		b = book;
		c = chapter;
		v = verse;
	}

	/**
	 * Returns the name of the book of this reference
	 * 
	 * @return the name of the reference's book
	 */
	public String getBook() {
		return b.toString();
	}

	/**
	 * Returns the book from the BookOfBible enum class
	 * 
	 * @return BookOfBible contained within this reference
	 */
	public BookOfBible getBookOfBible() {
		return b;
	}
	
	/**
	 * Returns the reference's chapter
	 * 
	 * @return the chapter residing inside this reference
	 */
	public int getChapter() {
		return c;
	}

	/**
	 * Returns the reference's verse
	 * 
	 * @return the verse bound by this reference
	 */
	public int getVerse() {
		return v;
	}

	/**
	 * This method should return the reference in the form: "book chapter:verse" For
	 * instance, "Genesis 2:3".
	 */
	@Override
	public String toString() {
		return getBook() + " " + Integer.toString(getChapter()) + ":" + Integer.toString(getVerse());
	}

	/**
	 * Should return true if and only if they have the same reference and text.
	 */
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Reference)) {
			return false;
		}
		Reference o = (Reference) other;
		if (!(o.getBookOfBible().equals(b))) {
			return false;
		} else if (o.getChapter() != c) {
			return false;
		} else if (o.getVerse() != v) {
			return false;
		}
		return true;
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public int compareTo(Reference otherRef) {
		int v1 = -1;
		int v2 = -1;
		int indexNo = -1;
		while (v1 == -1 || v2 == -1) {
			for (String s : BookOfBible.getBookNames()) {
				indexNo++;
				if (getBook() == s) {
					v1 = indexNo;
				}
				if (otherRef.getBook() == s) {
					v2 = indexNo;
				}
			}
		}
		if (v1 - v2 == 0) {
			if (c - otherRef.getChapter() == 0) {
				return v - otherRef.getVerse();
			} else {
				return c - otherRef.getChapter();
			}
		} else {
			return v1 - v2;
		}
	}
}
